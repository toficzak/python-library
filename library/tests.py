from django.test import TestCase
from django.urls import reverse

from .models import Book


class BookModelTest(TestCase):

    def setUp(self):
        Book.objects.create(title='just a test')

    def test_text_content(self):
        book=Book.objects.get(id=1)
        expected_object_name = f'{book.title}'
        self.assertEqual(expected_object_name, 'just a test')


class HomePageViewTest(TestCase):

    def setUp(self):
        Book.objects.create(title='this is another test')

    def test_view_url_exists_at_proper_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_by_name(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'home.html')
