from django.contrib import admin

from .models import Book
from .models import Author
from .models import Category
from .models import User

admin.site.register(Book)
admin.site.register(Author)
admin.site.register(Category)
admin.site.register(User)
