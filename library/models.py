from django.db import models


class User(models.Model):
    email = models.EmailField(blank=False, null=False)
    firstName = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)

    def __str__(self):
        return self.email


class Author(models.Model):
    firstName = models.CharField(max_length=20)
    lastName = models.CharField(max_length=50)
    publicationDate = models.DateField

    def __str__(self):
        return self.firstName + " " + self.lastName


class Category(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.TextField()
    authors = models.ManyToManyField(Author, verbose_name="List of authors", default=None, blank=True)
    isbn = models.CharField(max_length=30)
    borrowedBy = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    categories = models.ManyToManyField(Category, verbose_name="List of categories", default=None, blank=True)

    def __str__(self):
        return self.title[:50]


