Pipenv:
- shell

Migrating database:
- docker-compose run web python /code/manage.py migrate --noinput

Creating superuser:
- docker-compose run web python /code/manage.py createsuperuser

Creating library container:
- docker-compose up -d --build

Closing down:
- docker-compose down
